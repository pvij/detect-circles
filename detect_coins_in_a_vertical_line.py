import cv2 as cv
import os


def detect_circles(img_path):
    # load the image
    img = cv.imread(img_path, cv.IMREAD_COLOR)
    # check if image loaded fine
    if img is None:
        print("Error loading image")

    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    # Blurring using median filter
    gray = cv.medianBlur(gray, 5)

    rows = gray.shape[0]
    # print("rows | ", rows)
    circles = cv.HoughCircles(
        image=gray,
        method=cv.HOUGH_GRADIENT,
        dp=1,
        minDist=rows / 35,
        param1=75,
        param2=110,
        minRadius=0,
        maxRadius=0,
    )

    return img, [list(c) for c in circles[0]]


def display_circles(img, circles, output_file_path):
    if circles is not None:
        print("No of circles | ", len(circles))
        print(circles)
        for i in circles:
            center = (i[0], i[1])
            # centre
            cv.circle(img, center, 3, (0, 255, 0), 5)
            # circle outline
            radius = i[2]
            cv.circle(img, center, radius, (0, 0, 255), 2)
    cv.imwrite(output_file_path, img)


def get_coin_circles(circles):
    radii = [circle[2] for circle in circles]
    threshold_radius = max(radii) * 0.65
    return [circle for circle in circles if circle[2] > threshold_radius]


def check_if_x_coords_close(circle1, circle2):
    max_radius = max(circle1[2], circle2[2])
    x1, x2 = circle1[0], circle2[0]
    if abs(x1 - x2) <= 0.2 * max_radius:
        return True
    return False


def get_coins_falling_vertically(coin_circles, img):
    circles_left = coin_circles.copy()
    groups_of_circles_on_vertical_line = []
    for c1 in circles_left:
        circles_on_vertical_line = []
        circles_left.remove(c1)
        for c2 in circles_left:
            if check_if_x_coords_close(c1, c2):
                circles_on_vertical_line.append(c2)
                circles_left.remove(c2)
        if circles_on_vertical_line:
            circles_on_vertical_line.append(c1)
            groups_of_circles_on_vertical_line.append(circles_on_vertical_line)
    return groups_of_circles_on_vertical_line


def draw_lines(groups_of_circles_on_vertical_line, img, output_file_path):
    for grp in groups_of_circles_on_vertical_line:
        min_circle_vertically = min(grp, key=lambda c: c[1])
        max_circle_vertically = max(grp, key=lambda c: c[1])
        coord1 = (min_circle_vertically[0], min_circle_vertically[1])
        coord2 = (max_circle_vertically[0], max_circle_vertically[1])
        cv.line(img, coord1, coord2, (0, 0, 255), thickness=3)
    cv.imwrite(output_file_path, img)


img_dir = "images"
img_path = os.path.join(img_dir, "coin.jpg")
img, circles = detect_circles(img_path)
# display_circles(img.copy(), circles, output_file_path="output_with_circles.jpg")
coin_circles = get_coin_circles(circles)
display_circles(
    img.copy(),
    coin_circles,
    output_file_path=os.path.join(img_dir, "output_with_reqd_circles.jpg"),
)
groups_of_circles_on_vertical_line = get_coins_falling_vertically(
    coin_circles, img.copy()
)
# print(groups_of_circles_on_vertical_line)
# for i, grp in enumerate(groups_of_circles_on_vertical_line):
#     display_circles(
#         img.copy(),
#         grp,
#         output_file_path=os.path.join(
#             img_dir, "centres_on_vertical_line_" + str(i) + ".jpg"
#         ),
#     )
draw_lines(
    groups_of_circles_on_vertical_line,
    img.copy(),
    output_file_path=os.path.join(img_dir, "output.jpg"),
)
