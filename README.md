<h3>Circle Detection Using Hough Transform</h3>

The task is to draw a straight line through coins falling on a straight line in the image `coin.jpg`. HoughCircles from OpenCV has been used to detect the circles. The output with the circles drawn resides in `output_with_reqd_circles.jpg`. Output with the vertical lines passing through coins falling on a vertical line is in `output.jpg`.

Original Image:
![Original Image](images/coin.jpg)

Image with circles:
![Image with circles](images/output_with_reqd_circles.jpg)

Image with vertical lines through coins falling on a vertical line:
![Image with vertical lines through coins falling on a vertical line](images/output.jpg)
